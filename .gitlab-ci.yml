image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-operator-build-base

variables:
  # Configuration of K8s
  NAMESPACE: "n${CI_COMMIT_SHORT_SHA}"
  TAG: "${CI_COMMIT_SHORT_SHA}"
  HOSTSUFFIX: "n${CI_COMMIT_SHORT_SHA}"
  DOMAIN: "apps.ocp-ci-4623.k8s-ft.win"
  TLSSECRETNAME: "gitlab-ci-tls"
  # docker configuration
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375

stages:
  - prepare
  - test
  - release
  - functional_test
  - qa
  - cleanup
  - certification

.if_push_to_redhat: &if_push_to_redhat
  if: '$PUSH_TO_REDHAT == "true" '

.if_release_tag: &if_release_tag
  if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+(-(rc|RC|beta)\d*)?$/'

.skip_if_release_tag: &skip_if_release_tag
  <<: *if_release_tag
  when: never

default:
  interruptible: true

.cache:
  variables:
    GOPATH: "${CI_PROJECT_DIR}/.go"
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .go/pkg/mod/
    when: 'always'

pull_charts:
  stage: prepare
  script: scripts/retrieve_gitlab_charts.sh
  artifacts:
    paths:
      - charts/

lint_code:
  extends: .cache
  stage: test
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  script: golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  allow_failure: true
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

.test_job:
  extends: .cache
  stage: test
  variables:
    HELM_CHARTS: "${CI_PROJECT_DIR}/charts"
    GITLAB_OPERATOR_ASSETS: "${CI_PROJECT_DIR}/hack/assets"
  before_script:
    - mkdir coverage
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -skip 'controller' -cover -outputdir=coverage ./...

slow_unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -focus 'controller' -cover -outputdir=coverage ./...

.docker_build_job:
  extends: .cache
  stage: release
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    # Update module cache so it can be saved in CI cache (only the dependencies required to build)
    - docker run -v "${GOPATH}:/go" -v "${CI_PROJECT_DIR}:/code" -w /code golang:1.14 go list ./...
  interruptible: false

build_branch_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  except:
    refs:
      - master
      - tags

build_tag_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  rules:
    - <<: *if_release_tag

build_latest_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:latest" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:latest"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  only:
    refs:
      - master
      - tags

build_bundle_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}/bundle" -f Dockerfile.bundle .
    - docker push "${CI_REGISTRY_IMAGE}/bundle"
  only:
    refs:
      - master
    changes:
      - bundle/**/*

functional_tests:
  extends: .cache
  stage: functional_test
  script: 
     - CLEANUP="no" ./scripts/test.sh
     - echo "ROOT_PASSWORD=$(kubectl -n $NAMESPACE get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo)" >> functional_tests.env
     - echo "GITLAB_VERSION=$(kubectl -n $NAMESPACE exec -ti $(kubectl -n $NAMESPACE get pods | grep task-runner | awk '{print $1}') cat /srv/gitlab/VERSION)" >> functional_tests.env
  artifacts:
    reports:
      dotenv: functional_tests.env
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

qa:
  image: registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker
  stage: qa
  services:
  - docker:dind
  variables:
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
    QA_ARTIFACTS_DIR: $CI_PROJECT_DIR
    EE_LICENSE: $GITLAB_QA_EE_LICENSE
    QA_OPTIONS: "--tag smoke"
    QA_EXCLUDE_SPECS: "--exclude-pattern '**/*over_ssh_spec.rb'" # Remove when https://gitlab.com/gitlab-org/gl-openshift/gitlab-operator/-/issues/58 closed 
  script:
    - echo $QA_ENVIRONMENT_URL $GITLAB_VERSION
    - gem install gitlab-qa
    - SIGNUP_DISABLED=true QA_DEBUG=true GITLAB_USERNAME=root GITLAB_PASSWORD=$ROOT_PASSWORD GITLAB_ADMIN_USERNAME=root GITLAB_ADMIN_PASSWORD=$ROOT_PASSWORD gitlab-qa Test::Instance::Any EE:$GITLAB_VERSION $QA_ENVIRONMENT_URL -- $QA_OPTIONS $QA_EXCLUDE_SPECS
  dependencies:
    - functional_tests
  artifacts:
    when: on_failure
    expire_in: 7d
    paths:
    - ./gitlab-qa-run-*
  allow_failure: true
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

cleanup:
  extends: .cache
  stage: cleanup
  script: CLEANUP="only" ./scripts/test.sh
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'
      when: always

certification_upload:
  stage: certification
  image: "registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker:1.0.1"
  services:
    - docker:dind
  rules:
    - <<: *if_release_tag
    - <<: *if_push_to_redhat
  retry: 1
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - ruby scripts/push_to_redhat.rb "${CI_COMMIT_REF_NAME}"
